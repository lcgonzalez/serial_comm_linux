/* Copyright (C) 2015 Luis Carlos Gonzalez Garcia. */
/**
//  @file serial_comm.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 1.0//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  src: https://www.cmrr.umn.edu/~strupp/serial.html
//
//    This file is part of the Linux Serial Communication C Library.
//
//                          License Agreement
//             For the Linux Serial Communication C Library
//
//    Linux Serial Communication C Library is an open source library used to
//    simply communicate over the serial port in Linux.
//
//    Linux Serial Communication C Library is free software; you can redistribute
//    it and/or modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    Linux Serial Communication C Library is distributed in the hope that it
//    will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
//    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "serial_comm.h"

extern "C" {

int open_port(const char *portToUse)
{
  int fd; /* File descriptor for the port */


  fd = open(portToUse, O_RDWR | O_NOCTTY | O_NDELAY);
  /*
   * The O_NOCTTY flag tells UNIX that this program doesn't want to be the
   * "controlling terminal" for that port. If you don't specify this then any
   * input (such as keyboard abort signals and so forth) will affect your process.
   * Normally a user program does not want this behavior.
   *
   * The O_NDELAY flag tells UNIX that this program doesn't care what state the
   * DCD signal line is in - whether the other end of the port is up and running.
   * If you do not specify this flag, your process will be put to sleep until the
   * DCD signal line is the space voltage.
   */
  if (fd == -1)
  {
   /*
    * Could not open the port.
    */

    perror("open_port: Unable to open /dev/ttyf1 - ");
  }
  else
    block_when_read(fd, true);

  return (fd);
}

void block_when_read(int this_fd, bool block) {
    if(!block) {
        /* The FNDELAY option causes the read function to return 0 if no
         * characters are available on the port.
         * The read function can be made to return immediately by doing the following: */
        fcntl(this_fd, F_SETFL, FNDELAY);
    } else {
        /* To restore normal (blocking) behavior, call fcntl() without the FNDELAY option: */
        fcntl(this_fd, F_SETFL, 0);
    }
}

int set_baud_rate(int this_fd, speed_t speed){
    struct termios options;
    /* Get the current options for the port */
    tcgetattr(this_fd, &options);

    /* Set the baud rates (input and output) to speed */
    cfsetispeed(&options, speed);
    cfsetospeed(&options, speed);

    /* Enable the receiver and set local mode */
    /* The c_cflag member contains two options that should always be enabled,
     *CLOCAL and CREAD. */
    options.c_cflag |= (CLOCAL | CREAD);

    /* Set the new options for the port... */
    /* The TCSANOW constant specifies that all changes should occur immediately
     * without waiting for output data to finish sending or input data to finish
     * receiving. */
    return tcsetattr(this_fd, TCSANOW, &options);
}

int set_parity(int this_fd, int parity) {
    struct termios options;
    /* Get the current options for the port */
    tcgetattr(this_fd, &options);

    switch (parity) {
    case P8N1:
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;
        break;
    case P7E1 :
        options.c_cflag |= PARENB;
        options.c_cflag &= ~PARODD;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS7;

        /* You should enable input parity checking when you have enabled parity
         * in the c_cflag member (PARENB).
         * The revelant constants for input parity checking are INPCK, IGNPAR,
         * PARMRK , and ISTRIP. Generally you will select INPCK and ISTRIP to
         * enable checking and stripping of the parity bit:
         *
         * IGNPAR is a somewhat dangerous option that tells the serial driver to
         * ignore parity errors and pass the incoming data through as if no errors
         * had occurred. This can be useful for testing the quality of a communications
         * link, but in general is not used for practical reasons. */
        options.c_iflag |= (INPCK | ISTRIP);
        break;
    case P7O1 :
        options.c_cflag |= PARENB;
        options.c_cflag |= PARODD;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS7;

        options.c_iflag |= (INPCK | ISTRIP);
        break;
    }

    /* Set the new options for the port... */
    /* The TCSANOW constant specifies that all changes should occur immediately
     * without waiting for output data to finish sending or input data to finish
     * receiving. */
    return tcsetattr(this_fd, TCSANOW, &options);
}

int set_raw_mode(int this_fd, bool raw_mode){
    struct termios options;
    /* Get the current options for the port */
    tcgetattr(this_fd, &options);

    if(raw_mode) { /* Raw mode */
        /* Raw Onput */
        options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
        /* Raw Output */
        /* When the OPOST option is disabled, all other option bits in c_oflag are ignored. */
        options.c_oflag &= ~OPOST;
    } else {    /* Processed/Canonical mode */
        /* Canonical input */
        options.c_lflag |= (ICANON | ECHO | ECHOE);
        /* Processed Output */
        options.c_oflag |= OPOST;
    }

    /* Set the new options for the port... */
    /* The TCSANOW constant specifies that all changes should occur immediately
     * without waiting for output data to finish sending or input data to finish
     * receiving. */
    return tcsetattr(this_fd, TCSANOW, &options);
}

int set_sw_flowControl(int this_fd, bool sw_flowControl) {
    struct termios options;
    /* Get the current options for the port */
    tcgetattr(this_fd, &options);

    if(sw_flowControl) { /* Software flow control enabled */
        options.c_iflag |= (IXON | IXOFF | IXANY);
    } else {    /* Software flow control disabled */
        options.c_iflag &= ~(IXON | IXOFF | IXANY);
    }
}

} /* extern "C" */




