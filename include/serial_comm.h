/* Copyright (C) 2015 Luis Carlos Gonzalez Garcia. */
/**
//  @file serial_comm.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 1.0
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  src: https://www.cmrr.umn.edu/~strupp/serial.html
//
//    This file is part of the Linux Serial Communication C Library.
//
//                          License Agreement
//             For the Linux Serial Communication C Library
//
//    Linux Serial Communication C Library is an open source library used to
//    simply communicate over the serial port in Linux.
//
//    Linux Serial Communication C Library is free software; you can redistribute
//    it and/or modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    Linux Serial Communication C Library is distributed in the hope that it
//    will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
//    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/
//*/

#ifndef LCG_SERIAL_COMM_H
#define LCG_SERIAL_COMM_H

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

extern "C" {

/* No parity (8N1) */
#define P8N1 81
/* Even parity (7E1) */
#define P7E1 71
/* Odd parity (7O1) */
#define P7O1 701

/*
 * 'open_port()' - Open serial port 1.
 *
 * Returns the file descriptor on success or -1 on error.
 */
int open_port(const char *portToUse);
void block_when_read(int this_fd, bool block);
int set_baud_rate(int this_fd, speed_t speed);
int set_parity(int this_fd, int parity);
int set_raw_mode(int this_fd, bool raw_mode);
int set_sw_flowControl(int this_fd, bool sw_flowControl);

} /* extern "C" */

#endif /* LCG_SERIAL_COMM_H */
