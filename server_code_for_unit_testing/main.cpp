#include "serial_comm.h"

#include <stdio.h>
#include <stdlib.h> /* free() */

int main(int argc, char** argv) {

    int fd = open_port("/dev/ttyS0");

    set_baud_rate(fd, B9600); /* Set baud rate to 9600 b/s */
    set_parity(fd, P8N1); /* Set parity to "No parity 8 bits" */
    set_raw_mode(fd, true); /* Use raw mode */
    set_sw_flowControl(fd, false); /* Disable software flow control */

    char *data = 0;
    int dataLength=21;
    printf("Waiting for data!\n");
    while(1){
        read(fd, data, dataLength);
        int i;
        for(i=0; i<dataLength; i++){
            printf("%d",data[i]);
        }
        printf("\n");
        write(fd, data, 21);
        free(data);
        data = 0;
    }

    return 0;
}
