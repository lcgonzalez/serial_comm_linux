// Copyright (C) 2015 Luis Carlos Gonzalez Garcia.
/**
//  @file serial_comm_tests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the Linux Serial Communication C Library.
//
//                          License Agreement
//             For the Linux Serial Communication C Library
//
//    Linux Serial Communication C Library is an open source library used to
//    simply communicate over the serial port in Linux.
//
//    Linux Serial Communication C Library is free software; you can redistribute
//    it and/or modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    Linux Serial Communication C Library is distributed in the hope that it
//    will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
//    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "serial_comm.h"

#include <gmock/gmock.h>
#include <stdlib.h> /* free() */

class serial_comm_openAndClosePort_Test : public ::testing::Test{
public:

    const char* port_;
    int dataLength_;
    void SetUp() {
        port_ = "/dev/ttyS0";
        dataLength_ = 21;
    }
    void TearDown(){
    }
};

TEST_F(serial_comm_openAndClosePort_Test, SimpleOpenAndClose) {
    int fd = open_port(port_);
    int retVal = close(fd);
    ASSERT_EQ(0, retVal);
}

TEST_F(serial_comm_openAndClosePort_Test, SimpleSetBaudRate) {
    int fd = open_port(port_);
    int retValue = 0;
    retValue = set_baud_rate(fd, B9600);
    EXPECT_EQ(0, retValue);
    retValue = close(fd);
    ASSERT_EQ(0, retValue);
}

TEST_F(serial_comm_openAndClosePort_Test, SimpleSetParity) {
    int fd = open_port(port_);
    int retValue = 0;
    retValue = set_parity(fd, P8N1);
    EXPECT_EQ(0, retValue);
    retValue = close(fd);
    ASSERT_EQ(0, retValue);
}

TEST_F(serial_comm_openAndClosePort_Test, SimpleSetRawMode) {
    int fd = open_port(port_);
    int retValue = 0;
    retValue = set_raw_mode(fd, true);
    EXPECT_EQ(0, retValue);
    retValue = close(fd);
    ASSERT_EQ(0, retValue);
}

TEST_F(serial_comm_openAndClosePort_Test, SimpleSetSoftwareFlowControl) {
    int fd = open_port(port_);
    int retValue = 0;
    retValue = set_sw_flowControl(fd, true);
    EXPECT_EQ(0, retValue);
    retValue = close(fd);
    ASSERT_EQ(0, retValue);
}

TEST_F(serial_comm_openAndClosePort_Test, FullSimpleWriteAndRead) {
    int fd = open_port(port_);

    set_baud_rate(fd, B9600); // Set baud rate to 9600 b/s
    set_parity(fd, P8N1); // Set parity to "No parity 8 bits"
    set_raw_mode(fd, true); // Use raw mode
    set_sw_flowControl(fd, false); // Disable software flow control

    const char *expectedData = "hello world, this is1";
    write(fd, expectedData, dataLength_);
    char *actualData = 0;
    sleep(2);
    read(fd, actualData, dataLength_);
    ASSERT_STREQ(expectedData, actualData);
    free(actualData);
}

TEST_F(serial_comm_openAndClosePort_Test, FullComplexWriteAndRead) {
    int fd = open_port(port_);

    set_baud_rate(fd, B9600); // Set baud rate to 9600 b/s
    set_parity(fd, P8N1); // Set parity to "No parity 8 bits"
    set_raw_mode(fd, true); // Use raw mode
    set_sw_flowControl(fd, false); // Disable software flow control

    const char expectedData[21] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
    write(fd, expectedData, dataLength_);
    char *actualData = 0;
    sleep(2);
    read(fd, actualData, dataLength_);
    ASSERT_STREQ(expectedData, actualData);
    free(actualData);
}
