#include <gmock/gmock.h>

class NatNetLibraryTest : public ::testing::Test{
public:

    void SetUp() {
        ::testing::Test::SetUp();
    }
    void TearDown(){
        ::testing::Test::TearDown();
    }
};

TEST_F(NatNetLibraryTest, DownloadCompileAndExecuteTestFramework) {
    // If this assert is completed, the test framework has been downloaded in
    // the test project directory and it is working correctly.
    ASSERT_THAT(true, testing::Eq(true));
}

int main(int argc, char** argv) {
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
